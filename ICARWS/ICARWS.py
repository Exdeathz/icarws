import DonationReader as dr
import RelayController as relay
import argparse
import time
import json

class RideTheLightning:
    
    def __init__(self):
        ConfigFile = 'Conf.json'
        with open(ConfigFile) as Conf:
            self.config = json.load(Conf)

        if ('RELAY_CONF_FILE' in self.config):
            self.controller = relay.RelayController(self.config['RELAY_CONF_FILE'])
        else:
            self.controller = relay.RelayController()
        

        self.run = True

        self.donations = dr.DonationReader()
        self.threshold = self.config['ZAP_CONDITION']['ACCUMULATOR_ZAP_THRESHOLD']
        self.donation_cup = 0


        return

    def mainloop(self):

        while (self.run):
            
            if (self.config['DONATION_CHECK_MODE']['TYPE'].upper() == 'TEAM_TOTAL'):
                self.donations.UpdatePreviousValues() #update the previous values before attempting to update the cache.
                self.donations.CheckTeamLive() #update the values
                #check if we have more money in the donations that is less than the threashold and start to climb toward that.
                if (self.donations.cachedata['sumDonations'] > self.donations.PreviousSumDonations):
                    self.donation_cup += (self.donations.cachedata['sumDonations'] - self.donations.PreviousSumDonations)
            elif (self.config['DONATION_CHECK_MODE']['TYPE'].upper() == 'TEAM_INDIVIDUAL_DONATIONS'):
                self.donations.CheckIndividualDonation()


            if (self.config['TEST_MODE'].upper() == 'TRUE'):
                time.sleep(10)
                self.donations.cachedata['sumDonations'] += 100 #this line tests to make sure the relay is working
                self.donation_cup += 110


            if (self.config['ZAP_CONDITION']['TYPE'].upper() == 'EVAL'):
                if (eval(self.config['ZAP_CONDITION']['EVAL'])):
                    self.zap(self.controller)


            elif (self.donations.CallBackCondition(self.donations)):
                self.zap(self.controller)


    def zap(self, a):
        a.RelayOn()
        time.sleep(self.config['ZAP_DURATION_SECONDS'])
        a.RelayOff()

        self.donations.PreviousSumDonations = self.donations.cachedata['sumDonations']
        self.donation_cup -= self.threshold #We did a zap, so reduce the amount of accumulated donations by the amount that we have as the threshold.
        return



    


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='start the process of checking for donations, and zap if the donations are sufficient to trigger.')
    #parser.add_argument()
    args = parser.parse_args()
    print ('Arguments:')
    print (args)

    x = RideTheLightning()
    x.mainloop()


    pass