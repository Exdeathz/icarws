import gpiozero as gz
import json


class RelayController:
    """
    Class to provide the functionality to control the relay system.
    """

    def __init__(self, config_file = "RelayConfig.json"):

        print('Initializing...')

        self.prevalidated = False

        with open(config_file) as conf_file:
            self.Config = json.load(conf_file)
            print('Config loaded')
            print (self.Config)
        if (not self.ValidateRelayConfig()):
            print('Config Not Valid')
        else:
            self.ActiveRelays = self.ConfigureRelays()
            print(self.ActiveRelays)
            self.relaystates = {}
            for key in self.ActiveRelays.keys():
                self.relaystates[key] = False
            print(self.relaystates)

            

        
        return

    def RelayOn(self, RelayID = 0):
        relay_to_change = self.DecodeRelayID(RelayID)
        self.ActiveRelays[str(relay_to_change)].on()
        self.relaystates[str(relay_to_change)] = True
        return
    
    def RelayOff(self, RelayID = 0):
        relay_to_change = self.DecodeRelayID(RelayID)
        self.ActiveRelays[str(relay_to_change)].off()
        self.relaystates[str(relay_to_change)] = False
        return

    def IsSafe(self):

        return False
    
    def SetRelay(self, RelayID = 0, state = False):
        if (state):
            self.RelayOn(RelayID)
        else:
            self.RelayOff(RelayID)

        return
    
    def DecodeRelayID(self, RelayID):
        if (RelayID == 0):
            relay_to_change = self.Config['RELAYS_ENABLED'][0]
        elif (RelayID in self.Config['RELAYS_ENABLED']):
            relay_to_change = RelayID
        else:
            print('invald RelayID selected')
            return False
        
        return relay_to_change
    
    def ValidateRelayConfig(self):

        if (self.prevalidated):
            return True

        valid = True
        relayIDs = []
        for each in self.Config['PINS'].keys():
            relayIDs.append(int(each.split('_')[1]))
        for ActiveRelay in self.Config['RELAYS_ENABLED']:
            if (ActiveRelay in relayIDs):
                pass
            else:
                valid = False
        return valid

    def GetRelayPinFromRelayId(self, RelayID):
        if (self.ValidateRelayConfig()):
            if ('RELAY_{}_CONTROL_PIN'.format(RelayID) in self.Config['PINS'].keys()):
                return int(self.Config['PINS']["RELAY_{}_CONTROL_PIN".format(RelayID)][4:])



    def ConfigureRelays(self):
        relay_objects = {}
        if (self.ValidateRelayConfig()):
            for ActiveRelay in self.Config['RELAYS_ENABLED']:
                relay_objects[str(ActiveRelay)] = gz.OutputDevice(self.GetRelayPinFromRelayId(ActiveRelay), active_high=self.GetActiveHigh(), initial_value=self.GetDefaultPinState()) #default for relays should be false for active high and default pin state.

        return relay_objects

    def GetDefaultPinState(self):
        if (self.Config['DEFAULT_STATE'].upper() == "TRUE"):
            return True
        else:
            return False

    def GetActiveHigh(self):
        if (self.Config['ACTIVE_HIGH'].upper() == "TRUE"):
            return True
        else:
            return False


if __name__ == "__main__":

    x = RelayController()
    print("Relay Config:")
    print(x.Config)
    print("Active Relays:")
    print(x.ActiveRelays)
    print("Relay States:")
    print(x.relaystates)

    pass

