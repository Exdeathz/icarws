import requests, json


def printJSONresponse(r):
    """Prints the JSON from a response in a pretty human-readable format"""
    print ('{} {}\n'.format(json.dumps(r.json(), sort_keys=True, indent=4, separators=(',', ': ')), r))
    return


class RESTrequest:
    def __init__(s):
        # member variables.
        s.mode = 'GET'
        s.payload = ''
        s.responses = {}
        s.auth_type = 'TOKEN'
        s.token = ''
        s.username = ''
        s.password = ''
        s.url = ''
        s.headers = {}
        s.request_label = ''

        s.available_modes = ['GET', 'POST', 'PUT', 'HEAD', 'DELETE', 'OPTIONS']
        s.avail_auth_modes = ['TOKEN', 'BASIC']

        s.requests = {'GET': requests.get, 'POST': requests.post, 'DELETE': requests.delete, 'PUT': requests.put,
                      'HEAD': requests.head}
        return


    def run(s):
        if (s.auth_type == 'BASIC'):
            request = s.requests[s.mode]((s.url), headers=(s.headers), data=s.payload, auth=(s.username, s.password))
            s.responses[s.request_label] = request
        else:
            request = s.requests[s.mode]((s.url), headers=(s.headers), data=s.payload)
            s.responses[s.request_label] = request

        return