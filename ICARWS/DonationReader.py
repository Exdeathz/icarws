import time
import RequestBuilder
import dateutil

class DonationReader:
    """
    Get the status of donations and trigger a callback when donations are made that meet certain criteria.
    """

    def __init__(self,
    URL = 'https://extralife.donordrive.com',
    TeamID = 51702,
    callDelay = 15,
    Callback_Function = lambda : print('callback was called. Bzzap'), #simple lamda
    Callback_Condition_Evaluator_Function = lambda x: True if (x.cachedata['sumDonations'] > x.PreviousSumDonations) else False
    ):

        self.Queue = {}
        self.BaseURL = URL
        self.Team = TeamID
        self.DonationCount = 0
        self.DonationSum = 0
        self.TimeSinceLastCall = 0.0
        self.EnforcedDelay = callDelay
        self.cachedata = {}
        self.PreviousSumDonations = 0
        self.PreviousDonationCount = 0
        self.donation_last_check = {}
        self.donation_currentCheck = {}

        #seed the data
        self.cachedata = self.CheckTeamLive()
        self.PreviousDonationCount = self.cachedata['numDonations']
        self.PreviousSumDonations = self.cachedata['sumDonations']

        self.CallBackFunction = Callback_Function
        self.CallBackCondition = Callback_Condition_Evaluator_Function

        print(self.BaseURL)

        return
    
    def CheckTeamLive(self):
        CurrentTime = time.time()
        data = False
        
        if (CurrentTime - self.TimeSinceLastCall > self.EnforcedDelay):
            x = RequestBuilder.RESTrequest()
            x.url = '{}/api/teams/{}'.format(self.BaseURL, self.Team)
            x.mode = 'GET'
            x.headers['accept'] = 'application/json'
            '''
            docs recommend usign the ETag header and the if-not-modified header in order to improve caching and reduce network overhead.
            '''
            x.run()
            self.TimeSinceLastCall = time.time()

            print (x.responses[x.request_label])
            data = x.responses[x.request_label].json()
            print (data)
            self.cachedata = data
        else:
            print('Request Rate limted as per docs. Using cache instead.')

        return data
    
    def CallBack(self):
        if (self.CallBackCondition(self)):
            self.CallBackFunction()
        else:
            print('Callback condition not met, waiting for callback condition to be met...')
        return

    def UpdatePreviousValues(self):
        self.PreviousDonationCount = self.cachedata['numDonations']
        self.PreviousSumDonations = self.cachedata['sumDonations']

    def CheckIndividualDonation(self, threshold = 99):
        CurrentTime = time.time()
        data = False

        if (CurrentTime - self.TimeSinceLastCall > self.EnforcedDelay):
            x = RequestBuilder.RESTrequest()
            x.url = '{}/api/teams/{}/donations?where=amount>{}'.format(self.BaseURL, self.Team, threshold)
            x.mode = 'GET'
            x.headers['accept'] = 'application/json'
            x.run()
            self.TimeSinceLastCall = time.time()

            data = x.responses[x.request_label].json()
            self.donation_last_check = self.donation_currentCheck
            self.donation_currentCheck = data
            self.CheckNewIndividualDonations()
        return

    def CheckNewIndividualDonations(self):
        cutoff = -1

        for i in range(len(self.donation_currentCheck)):
            if (self.donation_currentCheck[i]['donationID'] == self.donation_last_check[0]['donationID']):
                cutoff = i
                break
        
        if (cutoff > -1):
            self.donation_currentCheck = self.donation_currentCheck[:cutoff]
        
        return


                


        


if __name__ == "__main__":
    x = DonationReader()
    x.CheckTeamLive()
    x.CallBack()

    x.cachedata['sumDonations'] += 1
    x.CallBack()
    pass
    
    